const winston = require('winston');
require('winston-daily-rotate-file');
const httpContext = require('express-http-context')

const format = winston.format;
const customAccessLevels = {
    levels: {
        http: -1,
        error: 0,
        warn: 1,
        info: 2,
        verbose: 4,
        debug: 5,
        silly: 6
    },
    colors: {
        error: 'red',
        warn: 'yellow',
        info: 'green',
        http: 'green',
        verbose: 'cyan',
        debug: 'blue',
        silly: 'magenta'
    }
};
const LOG_LEVEL = process.env.NODE_ENV === 'production' ? 'error' : 'debug'
winston.level = LOG_LEVEL;

winston.addColors(customAccessLevels.colors);
winston.loggers.add('logger', {
    level: LOG_LEVEL,
    format: format.printf(info => info.message),
    transports: [
        new winston.transports.DailyRotateFile({
            filename: `logs/app-%DATE%.log`,
            handleExceptions: false,
            datePattern: 'YYYY-MM-DD',
            maxFiles: '7d'
        })
    ],
    exitOnError: false
});

winston.loggers.add('uncatchException', {
    level: LOG_LEVEL,
    format: format.printf(info => JSON.stringify({
        environment: process.env.NODE_ENV,
        type: 'UncatchException',
        timestamp: new Date().toISOString(),
        logLevel: 'error',
        service: process.env.SERVICE_NAME,
        message: info.message
    })),
    transports: [
        new winston.transports.DailyRotateFile({
            filename: `logs/app-%DATE%.log`,
            handleExceptions: true,
            handleRejections: true,
            datePattern: 'YYYY-MM-DD',
            maxFiles: '7d'
        })
    ],
    exitOnError: false
})

winston.loggers.add('httpLogger', {
    level: LOG_LEVEL,
    levels: customAccessLevels.levels,
    format: format.printf(info => info.message),
    transports: [
        new winston.transports.DailyRotateFile({
            filename: `logs/access-%DATE%.log`,
            level: 'http', // Log only if info.level less than or equal to this level
            handleExceptions: false,
            datePattern: 'YYYY-MM-DD',
            maxFiles: '7d'
        })
    ],
    exitOnError: false
});


module.exports.logger = loggerWrapper(winston.loggers.get('logger'), 'CommonLogger');
module.exports.httpLogger = loggerWrapper(winston.loggers.get('httpLogger'), 'HttpLogger');


/**
 * override logger.info/error/debug,.... so that we can get more information as which function the log belongs to
 * @param {*} logger winston logger
 * @param {*} type logger type as http/access
 */
function loggerWrapper(logger, type) {
    const logLevels = ['http', 'error', 'warn', 'info', 'verbose', 'debug', 'silly']
    logLevels.forEach(logLevel => {
        const preFunction = logger[logLevel]
        logger[logLevel] = (message, params) => {
            const newMsg = {
                environment: process.env.NODE_ENV,
                type,
                timestamp: new Date().toISOString(),
                requestId: httpContext.get('requestId'),
                userId: httpContext.get('userId'),
                clientId: httpContext.get('clientId'),
                logLevel,
                service: process.env.SERVICE_NAME,
                ...params
            }

            if (logLevel === 'http') {
                try {
                    Object.assign(newMsg, JSON.parse(message))
                } catch (error) {
                    Object.assign(newMsg, { message })
                }
            }
            else Object.assign(newMsg, { message, stack: getStackTrace() })

            //if this exception belongs to a http-context then log requestInfo 
            if (logLevel === 'error' && newMsg.requestId) Object.assign(newMsg, { requestInfo: httpContext.get('requestInfo') })

            //convert messsage to string if it isn't a string (to avoid elasticsearch mapping field type conflict)
            if (newMsg.message && typeof newMsg.message !== 'string' && !(newMsg.message instanceof String)) newMsg.message = JSON.stringify(newMsg.message)

            preFunction(JSON.stringify(newMsg))
        }
    })
    return logger
}

//get stack trace of the current function 
function getStackTrace() {
    const error = new Error();
    const stack = error.stack
        .split("\n")
        .slice(2)
        .map((line) => line.replace(/\s+at\s+/, ""))
    stack.shift()
    return stack.join('\n')
}

//By default JSON.stringfy(<Error> err) will return {}, must add toJSON prototype to be able to convert ERROR to JSON
(function errorToJSON() {
    if (!('toJSON' in Error.prototype))
        Object.defineProperty(Error.prototype, 'toJSON', {
            value: function () {
                const alt = {};

                Object.getOwnPropertyNames(this).forEach(function (key) {
                    alt[key] = this[key];
                }, this);

                return alt;
            },
            configurable: true,
            writable: true
        });
})()